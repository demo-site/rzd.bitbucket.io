"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  var _$$lightGallery;

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.js-toggle-acc').on('click', function () {
    $(this).toggleClass('is-active');
    $(this).siblings().stop().slideToggle();
  });
  $('.b-clientsProjectsInner__slider').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 129 129" style="enable-background:new 0 0 129 129;" xml:space="preserve"><g><path d="M40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2,0-5.8l51-51l-51-51c-1.6-1.6-1.6-4.2,0-5.8s4.2-1.6,5.8,0l53.9,53.9c1.6,1.6,1.6,4.2,0,5.8L40.4,121.3z"/></g></svg>', '<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 129 129" style="enable-background:new 0 0 129 129;" xml:space="preserve"><g><path d="M40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2,0-5.8l51-51l-51-51c-1.6-1.6-1.6-4.2,0-5.8s4.2-1.6,5.8,0l53.9,53.9c1.6,1.6,1.6,4.2,0,5.8L40.4,121.3z"/></g></svg>']
  });
  var galleryArrow = '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path id="rght_copy" data-name="rght copy" class="cls-1" d="M680,2474a20,20,0,1,1,20-20A20.021,20.021,0,0,1,680,2474Zm0-38a18,18,0,1,0,18,18A18.018,18.018,0,0,0,680,2436Zm3.777,24.33a1.014,1.014,0,0,1-1.42,0,0.994,0.994,0,0,1,0-1.41l3.915-3.92H671.313a0.994,0.994,0,0,1-1-.99,1,1,0,0,1,1-1.01h14.968l-3.916-3.91a1,1,0,0,1,1.412-1.41l5.619,5.62a0.992,0.992,0,0,1,0,1.41Z"transform="translate(-660 -2434)"/></svg>';
  $('body').lightGallery((_$$lightGallery = {
    thumbnail: false,
    autoplayControls: false,
    fullScreen: false,
    download: false,
    counter: false,
    toogleThumb: false,
    thumbMargin: 10,
    thumbWidth: 72,
    thumbContHeight: 92,
    loop: false,
    hash: false,
    zoom: 0,
    actualSize: false,
    selector: '.lightGallery-item',
    prevHtml: galleryArrow,
    nextHtml: galleryArrow
  }, _defineProperty(_$$lightGallery, "thumbnail", false), _defineProperty(_$$lightGallery, "videojs", false), _$$lightGallery));
  $('.js-first-screen-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 129 129" style="enable-background:new 0 0 129 129;" xml:space="preserve"><g><path d="M40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2,0-5.8l51-51l-51-51c-1.6-1.6-1.6-4.2,0-5.8s4.2-1.6,5.8,0l53.9,53.9c1.6,1.6,1.6,4.2,0,5.8L40.4,121.3z"/></g></svg>', '<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 129 129" style="enable-background:new 0 0 129 129;" xml:space="preserve"><g><path d="M40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2,0-5.8l51-51l-51-51c-1.6-1.6-1.6-4.2,0-5.8s4.2-1.6,5.8,0l53.9,53.9c1.6,1.6,1.6,4.2,0,5.8L40.4,121.3z"/></g></svg>']
  });
  var fixedPanel = $('.js-fixed-panel');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 150 && fixedPanel.hasClass('fixed-panel-init')) {
      fixedPanel.addClass('fixed-panel-show');
    } else if ($(this).scrollTop() <= 200 && fixedPanel.hasClass('fixed-panel-init')) {
      fixedPanel.removeClass('fixed-panel-show');
    }
  });
  $('.remodal__inner .myForm').on('submit', function (event) {
    event.preventDefault();
    $('.remodal__notification').slideDown();
    $('.remodal__inner .myForm').slideUp();
    $('.remodal__title').slideUp();
  });
  $('.js-btn-burger').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');
    $('.mobile-panel .menu').toggleClass('menu-is-active');
    $('.mobile-panel__overlay-menu').toggleClass('overlay-is-active');
  });
  $.each($('.menu__nav-list').find('> li'), function (index, element) {
    if ($(element).find(' > ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('haschild').append(subMenuTrigger);
    }
  });
  $('.menu__nav-list .sub-menu-trigger').on('click', function (event) {
    $(this).toggleClass('rotade');

    if (!$(this).closest('li').find('>ul').length) {
      return;
    }

    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
  });
  $('.js-reviews-about-us-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    navText: ['<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 129 129" style="enable-background:new 0 0 129 129;" xml:space="preserve"><g><path d="M40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2,0-5.8l51-51l-51-51c-1.6-1.6-1.6-4.2,0-5.8s4.2-1.6,5.8,0l53.9,53.9c1.6,1.6,1.6,4.2,0,5.8L40.4,121.3z"/></g></svg>', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 129 129" style="enable-background:new 0 0 129 129;" xml:space="preserve"><g><path d="M40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2,0-5.8l51-51l-51-51c-1.6-1.6-1.6-4.2,0-5.8s4.2-1.6,5.8,0l53.9,53.9c1.6,1.6,1.6,4.2,0,5.8L40.4,121.3z"/></g></svg>']
  });
  $('.js-scroll-to-top').on('click', function () {
    $('body, html').animate({
      scrollTop: 0
    }, 800);
  });
}); // contacts map

var center1 = {
  lat: 55.807529,
  lng: 37.5683752
};

function initStandortMap() {
  var standortMap = document.getElementById('standort-map');

  if (!standortMap) {
    return;
  }

  var map = new google.maps.Map(standortMap, {
    center: {
      lat: center1.lat,
      lng: center1.lng
    },
    disableDefaultUI: true,
    zoom: 12
  });
  var marker2 = new google.maps.Marker({
    position: {
      lat: center1.lat,
      lng: center1.lng
    },
    map: map,
    icon: 'img/marker.png'
  });
  var infowindow = new google.maps.InfoWindow({
    pixelOffset: new google.maps.Size(0, 155)
  }); // Section map

  var objectPoint = new google.maps.LatLng(center1.lat, center1.lng),
      infowindow,
      service,
      markers = [];

  function createMarker(place, iconType) {
    // var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: map,
      icon: icon,
      position: place.geometry.location
    });
    marker.myType = myType;
    markers.push(marker);
  }
}

function initMap() {
  initStandortMap();
}