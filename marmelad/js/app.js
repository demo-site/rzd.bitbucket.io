/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = Object.assign({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: '',
  }, opts);

  let external = '';
  let typeClass = '';

  if (opts.mode === 'external') {
    external = `${opts.url}/sprite.${opts.type}.svg`;
  }

  if (opts.type !== 'icons') {
    typeClass = ` svg-icon--${opts.type}`;
  }

  opts.class = opts.class ? ` ${opts.class}` : '';

  return `
    <${opts.tag} class="svg-icon svg-icon--${name}${typeClass}${opts.class}" aria-hidden="true" focusable="false">
      <svg class="svg-icon__link">
        <use xlink:href="${external}#${name}"></use>
      </svg>
    </${opts.tag}>
  `;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

  'use strict';

  /**
   * определение существования элемента на странице
   */
  $.exists = (selector) => $(selector).length > 0;

  //=require ../_blocks/**/*.js

  $('.js-scroll-to-top').on('click', function () {
    $('body, html').animate({scrollTop:0},800);
  })
  
});


// contacts map
var center1 = {lat: 55.807529, lng: 37.5683752};

function initStandortMap() {
    var standortMap = document.getElementById('standort-map');

    if(!standortMap) {
        return;
    }

    var map = new google.maps.Map(standortMap, {
        center: {
            lat: center1.lat,
            lng: center1.lng
        },

        disableDefaultUI: true,
        zoom: 12,
    });

    var marker2 = new google.maps.Marker({
        position: {
            lat: center1.lat,
            lng: center1.lng
        },

        map: map,
        icon: 'img/marker.png',

    });


    var infowindow = new google.maps.InfoWindow({
        pixelOffset: new google.maps.Size(0,155)
    });
    

    // Section map
    var objectPoint = new google.maps.LatLng(center1.lat, center1.lng),
        infowindow,
        service,
        markers = [];

    function createMarker(place, iconType) {

        // var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: map,
            icon: icon,
            position: place.geometry.location
        });

        marker.myType = myType;
        markers.push(marker);

    }

}

function initMap() {
    initStandortMap();
}