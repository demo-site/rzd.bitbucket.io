var galleryArrow = '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path id="rght_copy" data-name="rght copy" class="cls-1" d="M680,2474a20,20,0,1,1,20-20A20.021,20.021,0,0,1,680,2474Zm0-38a18,18,0,1,0,18,18A18.018,18.018,0,0,0,680,2436Zm3.777,24.33a1.014,1.014,0,0,1-1.42,0,0.994,0.994,0,0,1,0-1.41l3.915-3.92H671.313a0.994,0.994,0,0,1-1-.99,1,1,0,0,1,1-1.01h14.968l-3.916-3.91a1,1,0,0,1,1.412-1.41l5.619,5.62a0.992,0.992,0,0,1,0,1.41Z"transform="translate(-660 -2434)"/></svg>';

$('body').lightGallery({
  thumbnail: false,
  autoplayControls: false,
  fullScreen: false,
  download: false,
  counter: false,
  toogleThumb: false,
  thumbMargin: 10,
  thumbWidth: 72,
  thumbContHeight: 92,
  loop: false,
  hash: false,
  zoom: 0,
  actualSize: false,
  selector: '.lightGallery-item',
  prevHtml: galleryArrow,
  nextHtml: galleryArrow,
  thumbnail: false,
  videojs: false,
});