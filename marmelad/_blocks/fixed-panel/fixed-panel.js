var fixedPanel = $('.js-fixed-panel');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 150 && fixedPanel.hasClass('fixed-panel-init')) {
      fixedPanel.addClass('fixed-panel-show');
    } else if ($(this).scrollTop() <= 200 && fixedPanel.hasClass('fixed-panel-init')) {
      fixedPanel.removeClass('fixed-panel-show');
    }
  });